
def isEqual(xs, i):
    if len(xs) % 2 != 0:
        return False
    zs = [1 for x in xs if "0" in x[i]]
    os = [1 for x in xs if "1" in x[i]]
    if len(zs) == len(os):
        return True
    else: 
        return False



def calcStuff(li, pat, fb, mode):
    arr = []
    for i in range(len(pat)):
        if len(arr) == 1:
            break
        key = pat[i] if i == 0 else (mostCom(arr,i) if mode == 0 else leastCom(arr,i))
        if isEqual((li if len(arr) == 0 else arr),i):
            key = fb
        arr = [x for x in (li if len(arr) == 0 else arr) if x[i:][0] == key]
    return arr[0]


def mostCom(li, i):
    t = trans(li)
    return '1' if t[i].count('1') > t[i].count('0') else '0'


def leastCom(li, i):
    t = trans(li)
    return '1' if t[i].count('1') < t[i].count('0') else '0'


def trans(li):
    x = list(zip(*li))
    return [list(y) for y in x]


def calcGa(li):
    ga = ""
    for l in li:
        ga += "1" if len([x for x in l if x == '1']) > len([x for x in l if x == '0']) else "0"
    return ga


def calcEp(ga):
    ep = ""
    for g in ga:
        ep+= "0" if "1" in g else "1"
    return ep


if __name__ == "__main__":
    with open("3/3.txt") as f:
        ls = [x.strip() for x in f.readlines()]
    
    t = trans(ls)

    ga = calcGa(t)
    print(">> ga: ",ga)
    ep = calcEp(ga)
    print(">> ep: ", ep)
    print("res: ",int(ep,2) * int(ga,2))
    print("\n")

    #part2
    ox = calcStuff(ls, ga, "1",0)
    print(">> ox: ", ox)
    co = calcStuff(ls, ep, "0",1)
    print(">> co: ",co)
    print("res2: ", int(ox,2) * int(co,2))

