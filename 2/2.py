
def getEm(lines):
    x = sum([int(w.split(" ")[1]) for w in lines if w.__contains__("forward")])
    du = sum([int(w.split(" ")[1]) for w in lines if w.__contains__("up")])
    dd = sum([int(w.split(" ")[1]) for w in lines if w.__contains__("down")])
    return x*(dd-du)

def getEm2(ls):
    a = x = d = 0
    for l in ls:
        if "up" in l:
            a -= int(l.split(" ")[1])
        elif "down" in l:
            a += int(l.split(" ")[1])
        else:
            val = int(l.split(" ")[1])
            x += val
            d += a * val
    return x*d

if __name__ == "__main__":

    with open("2/2.txt") as f:
        lines = f.readlines()
    x = getEm2(lines)
    print(x)