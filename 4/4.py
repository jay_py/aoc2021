import numpy as np

def readLines(path):
    with open(path) as f:
        return [x.strip() for x in f.readlines() if x.strip() != ""]


def parseLines(li):
    toDraw = [int(x) for x in li[0].split(',')]
    strBoards = li[1:]
    intBoards = []
    while len(strBoards) != 0:
        xs = strBoards[:5]
        li = []
        for i in range(5):
            li.append([int(x.strip()) for x in xs[i].split() if x.strip() != ""])
        intBoards.append(li)
        strBoards = strBoards[5:]
    return toDraw, intBoards


def checkBingo(li):
    for i in range(len(li)):
        for hline in li[i]:
            if all([x for _,x in hline]):
                return li[i],True
        for vline in trans(li[i]):
            if all([x for _,x in vline]):
                return li[i],True
    return [], False

def checkSingleBoard(board):
    for hline in board:
        if all([x for _,x in hline]):
            return True
    for vline in trans(board):
        if all([x for _,x in vline]):
            return True


def calcRev(li, n):
    return sum([x for x,f in np.concatenate(li) if not f]) * n


def mark(li, n):
    for board in li:
        for i in range(len(board)):
            board[i] = [ ((v,True) if v == n else (v,f)) for (v,f) in board[i] ]
    return li



def trans(li):
    x = list(zip(*li))
    return [list(y) for y in x]


def zipWithBool(li):
    #zs = [ [list( zip(y, [False for _ in range(len(y))]) for y in list(x)) ] for x in intBoards ]
    zipped = []
    for board in li:
        res = []
        for row in board:
            res.append(list(zip(row, [False for _ in range(len(row))])))
        zipped.append(res)
    return zipped


if __name__ == "__main__":
    toDraw, boards = parseLines(readLines("4/4.txt"))
    # Part 1
    zippedBoards = zipWithBool(boards)
    for i in range(len(toDraw)):
        zippedBoards = mark(zippedBoards, toDraw[i])
        board, won = checkBingo(zippedBoards)
        if won:
            print(calcRev(board, toDraw[i]))
            break
    
    # Part2
    for j in range(len(toDraw)):
        zippedBoards = mark(zippedBoards, toDraw[j])
        zippedBoards = [board for board in zippedBoards if not checkSingleBoard(board)]
        if len(zippedBoards) == 1:
            for k in range(j+1,len(toDraw)):
                zippedBoards = mark(zippedBoards, toDraw[k])
                if checkSingleBoard(zippedBoards[0]):
                    print(calcRev(zippedBoards[0], toDraw[k]))
                    break
            break
            
