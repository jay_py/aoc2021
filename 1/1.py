
def gt(li):
    return len([i for i in range(1, len(li)) if li[i] > li[i-1]])

if __name__ == "__main__":
    with open("1.txt") as f:
        inp = f.readlines()
    numbers = [int(x) for x in inp]
    x =  gt(numbers)
    print(">> a: ",x)
    numbers = [sum(numbers[x:x+3]) for x in range(len(numbers))]
    x = gt(numbers)
    print(">> b: ", x)
